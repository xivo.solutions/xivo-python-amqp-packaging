# xivo-python-amqp-packaging

Debian packaging for [amqp](https://amqp.readthedocs.org) used in XiVO.

## Upgrading

To upgrade amqp:

* Update the version number in the `VERSION` file
* Update the changelog using `dch -i` to the matching version
* Push the changes

